<?php
	include('config.php');
	include('session.php');
	ob_start();
	
	date_default_timezone_set('Asia/Kolkata');
    $today = date('Y-m-d', time());

    if($expiry_date < $today)
	{
		header('location:401.php');
	}
	
	global $random;
	$random = time();
	
	global $total;	
	$total=0.00;
	
	global $discount_total;	
	$discount_total=0.00;
	
	global $grand_total;	
	$grand_total=0.00;
	
	global $buy_points;
	$buy_points=0;

	global $tax_total;
	$tax_total=0.00;

	global $mrp_total;
	$mrp_total=0.00;

	global $total_after_discount;
	$total_after_discount=0.00;
	
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Transaction</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="/">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">Adminator</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
                    
					<li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="add_product.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Add product</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-orange-500 ti-list"></i> </span><span class="title">Stock Management</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-receipt"></i> </span><span class="title">Transaction</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-blue-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="reports.php"><span class="icon-holder"><i class="c-teal-500 ti-stats-up"></i> </span><span class="title">Reports</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-orange-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
						<li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> <?php echo ucwords($shop_name);?></a></li>
                        
                    </ul>
                    <ul class="nav-right">
                        
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20 " >
                                    <h4 class="c-grey-900 mB-20">Transaction </h4>
										<form method="POST">
											<div class="row">
												<div class="col-md-5 mb-3">
													<label for="validationCustom01">Product Code</label>
													<input type="text" class="typeahead form-control" placeholder="Enter product code" required name="prod_code" id="prod_code" autocomplete="off" autofocus>
													
												</div>
												<div class="col-md-5 mb-3">
													<label for="validationCustom02">Quantity</label>
													<input type="text" class="form-control" id="validationCustom02" placeholder="Enter product quantity" required name="quantity" id="quantity" value="1">
												</div>
												<div class="col-md-2 mb-3">
													<label for="validationCustom02">Add product</label>
													<input type="submit" name="scan" value="Add cart" class="form-control btn btn-dark">
												</div>
											</div>
											
										</form></br>
									<div class="table-responsive">
                                    <table class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product name</th>
												<th>Product code</th>
                                                <th>Quantity</th>
                                                <th>MRP</th>
                                                <th>Discount %</th>
                                                <th>Tax %</th>
												<th>Tax Type</th>
												<th>Total</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
										
										
                                        <tbody>
										<?php 
											if(isset($_POST['scan']) )
											{ 
												
												$barcode = mysqli_real_escape_string($conn,$_POST['prod_code']);
												$quantity = mysqli_real_escape_string($conn,$_POST['quantity']);
												
												$temp=$barcode;
												
												global $number;
												
												$sql = "SELECT * FROM tbl_products where product_code='$barcode' AND isActive='1'"; 
												$result=$conn->query($sql);
												
												if(!$result)
												{
													echo "Error : ".mysqli_error($conn);
												}
												else
												{
												
													if($result->num_rows > 0)
													{
													
														while($row=$result->fetch_assoc())
														{
														  $product_id=$row['id'];
														  $product_code=$row['product_code'];
														  $quan12=$row['quantity'];
														}
														
														$sql2="select * from tbl_seller_cart where product_code = '$barcode'";
														$result2=mysqli_query($conn,$sql2);
														
														$fetch_quan=0;
														while($row2=$result2->fetch_assoc())
														{
															$fetch_quan=$row2['quantity'];
														}
														
														
														if($quan12<=0)
														{
															echo '<script language="javascript">';
															echo 'alert("Product is out of stock"); location.href="transaction.php"';
															echo '</script>';	
														}
														else if($quantity>$quan12)
														{
															echo '<script language="javascript">';
															echo 'alert("You are adding product quantity more than available quantity."); location.href="transaction.php"';
															echo '</script>';
														}
														else if($fetch_quan>=$quan12)
														{
															echo '<script language="javascript">';
															echo 'alert("Product is out of stock"); location.href="transaction.php"';
															echo '</script>';
														}
														else
														{	
															$sql2="select * from tbl_seller_cart where product_code = '$barcode'";
															$result2=mysqli_query($conn,$sql2);
															
															if (!$result2) {
																printf("Error: %s\n", mysqli_error($conn));					
															}
														
															if($result2->num_rows > 0)
															{
																while($row=$result2->fetch_assoc())
																{
																	$existing_quantity=$row['quantity'];
																}
																
																$quantity=$quantity+$existing_quantity;
																
																$sql3="update tbl_seller_cart set quantity ='$quantity' where product_code = '$barcode'";
																$result3=mysqli_query($conn,$sql3);
																
																header("location:transaction.php");
															}
															else
															{
															
																$sql_ckeckout="INSERT INTO tbl_seller_cart (product_id, product_code, seller_id, quantity) VALUES ('$product_id','$product_code','$id','$quantity')";
																
																$result=mysqli_query($conn,$sql_ckeckout);
															
																if (!$result) {
																	printf("Error: %s\n", mysqli_error($conn));					
																}
																else
																{
																	header("location:transaction.php");
																}
																
															}
														}	
													}
													else
													{
														echo '<script language="javascript">';
														echo 'alert("Product not found"); location.href="transaction.php"';
														echo '</script>';
													}
												}

											}
											else
											{
															
															$sql1 = "SELECT * FROM tbl_seller_cart where seller_id='$id'";
															$result1=$conn->query($sql1);
															//$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
															if($result1->num_rows > 0)
															{
																$i=1;
																while($row1=$result1->fetch_assoc())
																{
																	$prod_id1=$row1['product_id'];
																	$sql = "SELECT * FROM tbl_products where id='$prod_id1'";
																	$result=$conn->query($sql);
																
											
																	
																	while($row=$result->fetch_assoc())
																	{
																	?>	
																	
																<?php
																							
																								$tax_type1=$row['tax_type'];
																								if($tax_type1==0)
																								{
																									$status ='<span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill">Inclusive</span>';
																								}
																								else if($tax_type1==1)
																								{
																									$status ='<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Exclusive</span>';
																								}
																						?>	
																							<tr>
																								<td><?php echo $i; ?></td>
																								<td><?php echo $row['name']; ?></td>
																								<td><?php echo $row['product_code']; ?></td>
																								<td><?php 
																										$quantity1=$row1['quantity'];
																										echo $quantity1; ?></td>
																								<td><?php 
																										$price=$row['price'];
																										$total=($total+$price)*$quantity1;
																										echo "&#8377 ".moneyFormatIndia(number_format((float)$price,2,'.','')); ?></td>
																								<td><?php
																										$discount_per=$row['discount_per'];
																										
																										echo $discount_per."%"; ?></td>
																										
																										
																								<td><?php
																										$tax_per=$row['tax_per'];
																										
																										$tax_type=$row['tax_type'];
																										echo $tax_per."%"; ?></td>
																								<td><?php echo $status;
																								?></td>
																								<td><?php
																										$total1=($price*($discount_per/100));
																										$discount_total=($discount_total+$total1)*$quantity1;
																										$total1=($price-$total1)*$quantity1;
																										
																										$total_after_discount=$total_after_discount+$total1;
																										
																										if($tax_type==0)
																										{
																											$temp1=number_format((float)$total1/(100+$tax_per)*100,2,'.','');
																											$temp1=($temp1*100.0)/100.0;
																											$mrp_total=$mrp_total+$temp1;
																											$temp2=number_format((float)($tax_per*$temp1)/100,2,'.','');
																											$tax_total=$tax_total+$temp2;
																											$grand_total=$grand_total+$total1;
																										}
																										else
																										{
																											$mrp_total=$mrp_total+$total1;
																											$temp2=number_format((float)($tax_per*$total1)/100,2,'.','');
																											
																											$tax_total=$tax_total+$temp2;
																											
																											$final_total=$total1+$temp2;
																											$grand_total=$grand_total+$final_total;
																										}


																										$buy_points=($buy_points+$row['buy_point'])*$quantity1;
																										echo "&#8377 ".moneyFormatIndia(number_format((float)$total1,2,'.',''));;
																								?></td>
																								
																								<td>
																								<div class="peers mR-15">
																									
																									<div class="peer">
																										<a href="delete_from_bill.php?id=<?php echo encrypt_url($row1['id']);?>&s_id=<?php echo encrypt_url($row1['seller_id']);?>" class="td-n c-red-500 cH-red-500 fsz-md p-5" onClick="return confirm('Please confirm deletion. Performed action cannot be undone.');"><i class="ti-trash"></i></a>
																									</div>
																								</div>
																								<?php //echo '<a href="delete_from_stock.php?id='. $row['id'].'" class="delete" style="text-decoration:none;" onClick="return confirm(\'Please confirm deletion\');"><i class="c-red-500 ti-trash xlarge"></i></a>';?>
																								</td>
																							</tr>
																<?php
																	
																	}
																	$i++;
																}
																	
																	
																}
																
											}
															
											
										?>		
												
                                            
                                            
                                        </tbody>
                                    </table>
									</div>
								</br><hr>	
								
								
								<div class="container text-center text-md-left">
      
              <!-- Footer links -->
              <div class="row text-center text-md-left mt-3 pb-3">
      
                  
                  <!--/.First column-->
				  <!--First column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Total </br>&#8377 <?php echo moneyFormatIndia(number_format((float)$total,2,'.',''));?></h6>
                      <p></p>
                  </div>
      
                  <hr class="w-100 clearfix d-md-none">
      
                  <!--Second column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Discount applied </br>&#8377 <?php echo moneyFormatIndia(number_format((float)$discount_total,2,'.',''));?></h6>
                      
                  </div>
				  
				  <!--First column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Subtotal <br/>&#8377 <?php echo moneyFormatIndia(number_format((float)$total_after_discount,2,'.',''));?></h6>
                      <p></p>
                  </div>

				  <!--Second column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Estimated Tax Collected / Applied<br/>&#8377 <?php echo moneyFormatIndia(number_format((float)$tax_total,2,'.',''));?></h6>
                      
                  </div>
                  <!--/.Second column-->
				  
      
                  <hr class="w-100 clearfix d-md-none">
      
                  <!--Third column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Total buy points to be earned </br><?php echo $buy_points?></h6>
                      
                  </div>
                  <!--/.Third column-->
      
                  <hr class="w-100 clearfix d-md-none">
      
                  <!--Fourth column-->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                      <h6 class="text-uppercase mb-4 font-weight-bold">Grand Total </br>&#8377 <?php echo moneyFormatIndia(number_format((float)$grand_total,2,'.',''));?></h6>
                      <div class="mT-30">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Proceed to checkout</button>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Complete transaction</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
												<form method="post">
													<div class="modal-body">
														<div class="row">
															<div class="col-md-12 mb-3">
																<label for="validationCustom01">Customer mobile number</label>
																<input type="number" autofocus class="typeahead form-control" placeholder="Enter customer mobile number" required name="cust_mobile" id="prod_code" autocomplete="off">
																
															</div>
												
															
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														<input type="submit" class="btn btn-primary" name="confirm" value="Complete Order">
													</div>
												</form>
<?php
	if(isset($_POST['confirm']))
	{
		$cust_mobile=mysqli_real_escape_string($conn,$_POST['cust_mobile']);
		
		$sql5 = "select * from tbl_users where mobile='$cust_mobile'";
		$result5=mysqli_query($conn,$sql5);

		$count = mysqli_num_rows($result5);

		if ($count == 1) 
		{ 
			while($row5=$result5->fetch_assoc())
			{
				$buyer_id=$row5['id'];
			}
		
			date_default_timezone_set('Asia/Kolkata');
			$date=date("Y-m-d H:i:s",time());
			$sql5 = "INSERT INTO tbl_transaction (seller_id, buyer_id, trans_date, 	sub_total, discount, trans_amount, status, buy_points) VALUES ('$id', '$buyer_id', '$date', '$total','$discount_total', '$grand_total', '','$buy_points')";
			$result5=mysqli_query($conn,$sql5);
			$last_id = mysqli_insert_id($conn);
			if(!$result5)
			{
				echo "".mysqli_error($conn);
			}
			else
			{
				
				$sql1="select * from tbl_seller_cart where seller_id='$id'";
				$result1=mysqli_query($conn,$sql1);
				if(!$result1)
				{
					echo ''.mysqli_error($conn);
				}
				else
				{
					while($row1=$result1->fetch_assoc())
					{
						$prod_id1=$row1['product_id'];
						$quan=$row1['quantity'];
						
						$sql = "SELECT * FROM tbl_products where id='$prod_id1'";
						$result=$conn->query($sql);
						
						while($row=$result->fetch_assoc())
						{
							$prod_id2=$row['id'];
							$name=$row['name'];
							$prod_code=$row['product_code'];
							$price=$row['price'];
							$discount_per=$row['discount_per'];
							$buy_point=$row['buy_point']*$quan;
							
							$total1=($price*($discount_per/100));
							//$discount_total=$discount_total+$total1;
							$total1=($price-$total1)*$quan;
							
							$sql2 = "INSERT INTO tbl_transaction_products VALUES ('$last_id','$date','$prod_id2','$name','$prod_code','$id','$price','$discount_per', '$quan', '$total1','$buy_point')";
							$result2=mysqli_query($conn,$sql2);
							
							if(!$result2)
							{
								echo "".mysqli_error($conn);
							}
							else
							{						
								$sql3="select * from tbl_products where id='$prod_id2'";
								$result3=mysqli_query($conn,$sql3);
								while($row2=$result3->fetch_assoc())
								{
									$fetched_quan=$row2['quantity'];
									$updated_quan=$fetched_quan-$quan;

									$sql4="update tbl_products set quantity='$updated_quan' where id='$prod_id2'";
									$result4=mysqli_query($conn,$sql4);
								}
							}
						}
						
					}
					
					require('u/fpdf.php');

					class PDF extends FPDF
					{
						function Header()
						{
							if(!empty($_FILES["file"]))
							{
								$uploaddir = "temp/test.png";
								$nm = $_FILES["file"]["name"];
								$random = rand(1,99);
								move_uploaded_file($_FILES["file"]["tmp_name"], $uploaddir.$random.$nm);
								$this->Image($uploaddir.$random.$nm,10,10,20);
								unlink($uploaddir.$random.$nm);
							}
							$this->SetFont('Arial','B',12);
							$this->Ln(1);
						}
						function Footer()
						{
							$this->SetY(-15);
							$this->SetFont('Arial','I',8);
							$this->Cell(0,10,'Reg. Address: KK Market, Pune Satara Road, Katraj, Pune - 411041',0,0,'L');
							$this->Cell(0,10,'ISEBY | You see it, You buy it',0,0,'R');
						}
						function ChapterTitle($num, $label)
						{
							$this->SetFont('Arial','',12);
							$this->SetFillColor(200,220,255);
							$this->Cell(0,10,'ISBEY Solutions',0,0,'R',true);
							$this->Ln(0);
						}
						function ChapterTitle2($num, $label)
						{
							$this->SetFont('Arial','',12);
							$this->SetFillColor(249,249,249);
							$this->Cell(0,6,"$num $label",0,1,'L',true);
							$this->Ln(0);
						}

						function WordWrap(&$text, $maxwidth)
						{
							$text = trim($text);
							if ($text==='')
							return 0;
							$space = $this->GetStringWidth(' ');
							$lines = explode("\n", $text);
							$text = '';
							$count = 0;

							foreach ($lines as $line)
							{
								$words = preg_split('/ +/', $line);
								$width = 0;

								foreach ($words as $word)
								{
									$wordwidth = $this->GetStringWidth($word);
									if ($wordwidth > $maxwidth)
									{
										// Word is too long, we cut it
										for($i=0; $i<strlen($word); $i++)
										{
											$wordwidth = $this->GetStringWidth(substr($word, $i, 1));
											if($width + $wordwidth <= $maxwidth)
											{
												$width += $wordwidth;
												$text .= substr($word, $i, 1);
											}
											else
											{
												$width = $wordwidth;
												$text = rtrim($text)."\n".substr($word, $i, 1);
												$count++;
											}
										}
									}
									elseif($width + $wordwidth <= $maxwidth)
									{
										$width += $wordwidth + $space;
										$text .= $word.' ';
									}
									else
									{
										$width = $wordwidth + $space;
										$text = rtrim($text)."\n".$word.' ';
										$count++;
									}
								}
								$text = rtrim($text)."\n";
								$count++;
							}
							$text = rtrim($text);
							return $count;
						}
					}


					$pdf = new PDF();
					$pdf->AliasNbPages();

					$pdf->AddPage();
					$pdf->SetFont('Arial','B',10);
					$pdf->SetTextColor(32);
					
					$image1 = "assets/static/images/logo_iseby.png";
					$pdf->Cell( 100, 20, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );

					
					$pdf->Cell(0,5,'ISEBY Solutions',0,1,'R');
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(0,5,'GSTIN No. : ABCDE12345WXYZ0',0,1,'R');
					$pdf->Cell(0,5,'',0,1,'L');
					$pdf->Cell(0,3,'',0,1,'L');
					


					$pdf->Cell(190,0,'',1,1,'L');
					
					$pdf->SetFont('Arial','B',12);
					$pdf->Cell(90,10,'Date :- '.date('d/m/Y',time()),0,0,'L');
					$pdf->SetFont('Arial','B',18);
					$pdf->Cell(80,10,'Tax Invoice',0,1,'R');
					$pdf->SetFont('Arial','B',12);
					$pdf->Cell(190,3,'',0,1,'L');
					$pdf->Cell(95,5,'Buyer name & address',0,0,'L');
					$pdf->Cell(95,5,'Seller name & address',0,1,'L');
					$pdf->SetFont('Arial','',10);



					$x = $pdf->GetX();
					$y = $pdf->GetY();

					$sql2 = "SELECT * FROM tbl_users WHERE mobile='$cust_mobile'";
					$result2=mysqli_query($conn,$sql2);
					$row2=$result2->fetch_assoc();
					
					$col1="".ucwords($row2['name'])."\n".$row2['mobile'];
					$pdf->MultiCell(95, 5, $col1, 0, 1);

					$pdf->SetXY($x + 95, $y);


					$sql3 = "SELECT * FROM tbl_users WHERE id='$id'";
					$result3=mysqli_query($conn,$sql3);
					$row3=$result3->fetch_assoc();

					$sql4 = "SELECT * FROM tbl_seller_info WHERE id='$id'";
					$result4=mysqli_query($conn,$sql4);
					$row4=$result4->fetch_assoc();


					$col2="".ucwords($row3['name'])."\n".ucwords($row4['shop_address'])."\n".$row3['mobile'];
					$pdf->MultiCell(95, 5, $col2, 0);
					$pdf->Ln(0);

					$pdf->Cell(0,5,'',0,1,'L');
					$pdf->SetFillColor(224,235,255);
					$pdf->SetDrawColor(0,0,0);
					$pdf->SetFont('Arial','B',10);
					
					$pdf->Cell(7,6,'#','BT',0,'L',0);
					$pdf->Cell(45,6,'Product name','BT',0,'L',0);
					$pdf->Cell(25,6,'Product code','BT',0,'L',0);
					$pdf->Cell(17,6,'Quantity','BT',0,'L',0);
					$pdf->Cell(25,6,'MRP','BT',0,'L',0);
					$pdf->Cell(23,6,'Discount %','BT',0,'L',0);
					$pdf->Cell(15,6,'Tax %','BT',0,'L',0);
					$pdf->Cell(25,6,'Total','BT',0,'L',0);
					$pdf->Cell(8,6,'','BT',1,'L',0);

					$pdf->SetFont('Arial','',10);
					$sql1="select * from tbl_seller_cart where seller_id='$id'";
					$result1=mysqli_query($conn,$sql1);
					//$row=mysqli_fetch_array($result);
					if($result1->num_rows > 0)
					{
						$i=1;
						$a=0;
						$b=0;
						$c=0;
						while($row1=$result1->fetch_assoc())
						{
							$prod_id1=$row1['product_id'];
							$sql = "SELECT * FROM tbl_products where id='$prod_id1'";
							$result=$conn->query($sql);
						

							
							while($row=$result->fetch_assoc())
							{
								$pdf->Cell(7,6,$i,'',0,'L',0);
								$pdf->Cell(45,6,ucwords(substr($row['name'],0,20)),'',0,'L',0);
								$pdf->Cell(25,6,$row['product_code'],'',0,'L',0);
								$quantity1=$row1['quantity'];
								$pdf->Cell(17,6,$quantity1,'',0,'L',0);
								$price=$row['price'];
								$a=$a+$price;
								$pdf->Cell(25,6,moneyFormatIndia(number_format((float)$price,2,'.','')),'',0,'L',0);
								$discount_per=$row['discount_per'];
								$pdf->Cell(23,6,$discount_per.'%','',0,'L',0);
								$total1=($price*($discount_per/100));
								$b=$b+$total1;
								$total1=($price-$total1)*$quantity1;
								$c=$c+$total1;
								
								$tax_type2=$row['tax_type'];
								if($tax_type2==0)
								{
									$print_tax="$";
								}
								else
								{
									$print_tax="#";
								}

								$pdf->Cell(15,6,$row['tax_per']." %",'',0,'L',0);
								$pdf->Cell(25,6,moneyFormatIndia(number_format((float)$total1,2,'.','')),'',0,'L',0);
								$pdf->Cell(8,6,$print_tax,'',1,'L',0);
							}
							$i++;
						}
					}


					$pdf->SetDrawColor(0,0,0);
					//$pdf->Cell(0,2,'',0,1,'L');
					$pdf->Cell(190,0,'',1,1,'L');
					$pdf->Cell(0,2,'',0,1,'L');
					$pdf->Cell(0,5,'Total : '.moneyFormatIndia(number_format((float)$total,2,'.','')),0,1,'R');
					$pdf->Cell(0,5,'Discount : '.moneyFormatIndia(number_format((float)$discount_total,2,'.','')),0,1,'R');
					$pdf->Cell(0,5,'Sub Total : '.moneyFormatIndia(number_format((float)$total_after_discount,2,'.','')),0,1,'R');
					$pdf->Cell(0,5,'CGST : '.moneyFormatIndia(number_format((float)$tax_total/2,2,'.','')),0,1,'R');
					$pdf->Cell(0,5,'SGST : '.moneyFormatIndia(number_format((float)$tax_total/2,2,'.','')),0,1,'R');

					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(0,5,'Grand Total : '.moneyFormatIndia(number_format((float)$grand_total,2,'.','')),0,1,'R');
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(0,5,'Amount in words: '.ucwords(getIndianCurrency($grand_total)).' Only',0,1,'L');
					$pdf->Cell(190,0,'',1,1,'L');

					$pdf->SetFont('Arial','',8);

					$pdf->Cell(40,5,'$ = Inclusive tax',0,0,'L');
					$pdf->Cell(40,5,'# = Exclusive tax',0,1,'L');

					$filename="pdf/".$last_id.".pdf";
					$pdf->Output($filename,'F');

					$sql6="delete from tbl_seller_cart where seller_id='$id'";
					$result6=mysqli_query($conn,$sql6);
					header('location:transaction.php');
				}
			}
		}
		else
		{
			echo '<script language="javascript">';
			echo 'alert("Enter mobile number not yet registered."); location.href="transaction.php"';
			echo '</script>';
		}
	}
	ob_end_flush();
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                  </div>
                  <!--/.Fourth column-->
              </div>
      
          </div>
      
								
								
								
								
								
								
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            
      <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © <?php echo date('Y',time());?> ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
	<script>
    $(document).ready(function () {
        $('#prod_code').typeahead({
            source: function (query, result) {
                $.ajax({
                    url: "search_product.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>
<style> .typeahead {
    
    
    
    max-width: 100%;
    min-width: 95%;
    
    color: #000;
}

.tt-menu {
    width: 300px;
}

ul.typeahead {
    margin: 0px;
    padding: 10px 0px;
}

ul.typeahead.dropdown-menu li a {
    padding: 10px !important;
    border-bottom: #CCC 1px solid;
    color: #333;
}

ul.typeahead.dropdown-menu li:last-child a {
    border-bottom: 0px !important;
}



.dropdown-menu>.active>a,
.dropdown-menu>.active>a:focus,
.dropdown-menu>.active>a:hover {
    text-decoration: none;
    background-color: #dcdcdc;
    outline: 0;
	
	
}

</style>
</body>

</html>

<?php

function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    
	return ($Rupees ? $Rupees . 'Rupees' : '') . $paise ;
}


function encrypt_url($string) {
  $key = "MAL_979877"; //key to encrypt and decrypts.
  $result = '';
  $test = "";
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));

     //$test[$char]= ord($char)+ord($keychar);
     $result.=$char;
   }

   return urlencode(base64_encode($result));
}
function moneyFormatIndia($num){

$explrestunits = "" ;
$num=preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des="00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}
?>