<?php
ob_start();

include('config.php');    
include('session.php');


require 'src/Cloudinary.php';
require 'src/Uploader.php';
require 'src/Api.php';

    if(!$_SESSION['login_user'])
    {
		header('location:login.php');
    }

$prod_id=decrypt_url($_GET['id']);
$seller_id=decrypt_url($_GET['s_id']);


function decrypt_url($string) {
    $key = "MAL_979877"; //key to encrypt and decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)-ord($keychar));
     $result.=$char;
   }
   return $result;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!--<link 
 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
 rel="stylesheet"
integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" 
 crossorigin="anonymous"
>-->
    <title>Add product</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="index.html">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISEBY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
				
                    <li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Products</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="sellers.php"><span class="icon-holder"><i class="c-blue-500 ti-user"></i> </span><span class="title">Sellers</span></a></li>
                    
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-purple-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                        <li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> Super Admin </a></li>
                    </ul>
                    <ul class="nav-right">
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    
						<div class="row gap-20 masonry pos-r" >
							<div class="masonry-sizer col-md-6"></div>
							<div class="masonry-item col-md-12">
                              <div class="bgc-white p-20 bd">
                                  <?php
										$sql = "SELECT * FROM tbl_products WHERE id ='$prod_id'";
										$result1 = mysqli_query($conn,$sql);
										if(!$result1)
										{
											echo "".mysqli_error($conn);
										}
										while($row = mysqli_fetch_array($result1,MYSQLI_ASSOC)){;
									?>
                                    
                                <div class="container" id="product-section">
                                  <div class="row">
                                   <div class="col-md-6">
                                    <div class="w3-content" style="width:100%">
                                        <?php
                                            $sql2 = "SELECT * FROM tbl_media WHERE product_id ='$prod_id' AND type='1'";
                                            $result2 = mysqli_query($conn,$sql2);
                                            if(!$result2)
                                            {
                                                echo "".mysqli_error($conn);
                                            }
                                            while($row2 = mysqli_fetch_array($result2,MYSQLI_ASSOC))
                                            {
                                                
                                                echo '<img class="mySlides" src="'.$row2['file_name'].'" style="width:100%">';
                                            }
                                        ?>
                                      
                                      <div class="w3-row-padding w3-section">
                                          <?php
										  $i=1;
										  $sql3 = "SELECT * FROM tbl_media WHERE product_id ='$prod_id' AND type='1'";
                                          $result3 = mysqli_query($conn,$sql3);
                                          while($row3 = mysqli_fetch_array($result3,MYSQLI_ASSOC))
                                            {
                                                echo '<div class="w3-col s4">
                                          <img class="demo w3-opacity w3-hover-opacity-off" src="'.$row3['file_name'].'" style="width:100%" onclick="currentDiv('.$i.')">
                                        </div>';
										$i++;
                                            }
                                        ?>
                                        
                                      </div>
                                    </div>

                                   </div>
                                   <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-md-12">
                                           <h1><?php echo $row['name'];?></h1>
                                         </div>
                                           <div class="col-md-12">
                                               <?php $quan=$row['quantity'];
														if($quan>50)
														{
															$status ='<span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="Greater than 50 quantity available">Available</span>';
														}
														else if($quan<=50 && $quan>0)
														{
															$status ='<span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="Less than 50 quantity available">Running low</span>';
														}
														else if($quan==0)
														{
															$status ='<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="0 quantity available">Out of stock</span>';
														}
                                                        echo $status;                                        
                                                    ?>
                                               </div>
                                         </div>
                                       <div class="row">
                                         <div class="col-md-12 bottom-rule">
                                          <h1 class="product-price"><?php $price=$row['price'];
                                              $discount_per=$row['discount_per'];
                                              if($discount_per>0)
                                              {
                                                  $discount=$price*($discount_per/100);
                                                  $total=$price-$discount;
                                                  echo '<span style="font-size:12px; color:green;">Special Price</span></br> &#8377 '.$total. '&nbsp;<strike style="font-size:14px;">&#8377 '.$price.'</strike>&nbsp;<span style="font-size:16px; color:green;">'.$discount_per. '% off</span>' ;
                                              }?>
											  </h1>
                                         </div>
                                        </div>
                                       <div class="row">
                                         <div class="col-md-12 bottom-rule">
                                          <h2>Description</h2>
										  <p style="text:align:justify;">
										  <?php echo $row['description']?>
										  </p>
                                         </div>
                                        </div>
                                    <?php } ?>   
                                  </div>
                                 </div><!-- end container -->
								 
								 <div class="row">
									<div class="col-md-12">	
									</div>
								 </div>
                            </div>
                           </div>
						</div>
					
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © 2017 ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
	
	
    <script>
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
          showDivs(slideIndex += n);
        }

        function currentDiv(n) {
          showDivs(slideIndex = n);
        }

        function showDivs(n) {
          var i;
          var x = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("demo");
          if (n > x.length) {slideIndex = 1}
          if (n < 1) {slideIndex = x.length}
          for (i = 0; i < x.length; i++) {
             x[i].style.display = "none";
          }
          for (i = 0; i < dots.length; i++) {
             dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
          }
          x[slideIndex-1].style.display = "block";
          dots[slideIndex-1].className += " w3-opacity-off";
        }
</script>
	
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	


</body>
</html>