<?php
ob_start();
ini_set("log_errors", 1);
ini_set("error_log", "tmp/php-error.log");

include('config.php');    
include('session.php');


$seller_id=decrypt_url($_GET['s_id']);


function decrypt_url($string) {
    $key = "MAL_979877"; //key to encrypt and decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)-ord($keychar));
     $result.=$char;
   }
   return $result;
}


	date_default_timezone_set('Asia/Kolkata');
	$today = date('Y-m-d', time());
	
    if(!$_SESSION['login_user'])
    {
		header('location:login.php');
    }
	
	global $u;
	$u = time();
	
	
	global $error;

	global $number;
	
	if(isset($_POST['submit'])) {
	
	  
	  $prod_name = mysqli_real_escape_string($conn,$_POST['prod_name']);
	  $prod_desc = mysqli_real_escape_string($conn,$_POST['prod_desc']);
    
	  $prod_rate=mysqli_real_escape_string($conn,$_POST['prod_rate']);
	  $prod_quan=mysqli_real_escape_string($conn,$_POST['prod_quan']);
	  $discount_per=mysqli_real_escape_string($conn,$_POST['discount_per']);
	  
	  $category=mysqli_real_escape_string($conn,$_POST['category']);
      $sub_category=mysqli_real_escape_string($conn,$_POST['sub_category']);
      
      $tax_type=mysqli_real_escape_string($conn,$_POST['tax_type']);
	  $tax_cat=mysqli_real_escape_string($conn,$_POST['tax_cat']);
	  $tax_per=mysqli_real_escape_string($conn,$_POST['tax_per']);

	  if(isset($_POST['isshipping']))
	  {
          $isshipping=mysqli_real_escape_string($conn,$_POST['isshipping']);
          $inter_charges=mysqli_real_escape_string($conn,$_POST['inter_charges']);
          $out_charges=mysqli_real_escape_string($conn,$_POST['out_charges']);
          $state_charges=mysqli_real_escape_string($conn,$_POST['state_charges']);
	  }
	  else
	  {
          $isshipping=0;
          $inter_charges=0;
          $out_charges=0;
          $state_charges=0;
	  }
	  
	  
	  
	  $buy_point=mysqli_real_escape_string($conn,$_POST['buy_point']);
	  $refer_point=mysqli_real_escape_string($conn,$_POST['refer_point']);
     
	 	  
	   if (empty($prod_name) || empty($prod_desc) || empty($prod_rate) || empty($discount_per) || empty($category) || empty($sub_category)||empty($buy_point)||empty($refer_point) )
	  {
		  $error="Please fill all the manditory fields.";
		  
		  
	  }
	  else 
	  {
		  
		  $barcode_color="#000000";
		  $sql2 = "update tbl_products SET  name='$prod_name', description='$prod_desc', seller_id='$id', quantity='$prod_quan', price='$prod_rate', discount_per='$discount_per', barcode_color='$barcode_color', category_id='$category', sub_category='$sub_category', created_at='$u', modified_at='$u', isActive=1, sgstid=1, city_shipping='$isshipping', city_charges='$inter_charges', out_shipping='$isshipping', out_charges='$out_charges', state_shipping='$isshipping', state_charges='$state_charges', buy_point='$buy_point', refer_point='$refer_point', tax_type='$tax_type', tax_cat='$tax_cat', tax_per='$tax_per' WHERE id='$prod_id'";
		  
		  
		  $result2=mysqli_query($conn,$sql2);
		  
		  
		  if(!$result2)
		  {
			echo "Error:".mysqli_error($conn);
		  }
		  else
		  {  
			echo '<script language="javascript">';
			echo 'alert("Product information updated successfully"); location.href="stock.php"';
			echo '</script>';
				
		  }

	  }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Edit product</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="index.html">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISEBY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
				
                    <li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Products</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="sellers.php"><span class="icon-holder"><i class="c-blue-500 ti-user"></i> </span><span class="title">Sellers</span></a></li>
                    
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-purple-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                        <li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> Super Admin </a></li>
                    </ul>
                    <ul class="nav-right">
                        
                        
                    <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    
						<div class="row gap-20 masonry pos-r" >
							<div class="masonry-sizer col-md-6"></div>
							<div class="masonry-item col-md-12">
                            <div class="bgc-white p-20 bd">
                                <h4 class="c-grey-900">Edit product </h4>
                                <div class="mT-30">
                                    <form class="container" id="needs-validation" novalidate method="POST" enctype="multipart/form-data">
									<?php
										$sql = "SELECT * FROM tbl_users WHERE id ='$seller_id'";
										$result1 = mysqli_query($conn,$sql);
										if(!$result1)
										{
											echo "".mysqli_error($conn);
										}
										while($row = mysqli_fetch_array($result1,MYSQLI_ASSOC)){;
									?>


                                            


                                        <div class="row">
                                            <div class="col-md-4 mb-3">
                                                <label for="validationCustom01">Name</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter name" required name="prod_name" value="<?php echo  $row['name']; ?>" autofocus>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="validationCustom02">Email address</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter product description" required name="prod_desc" value="<?php echo  $row['email']; ?>">
												
                                            </div>
											<div class="col-md-4 mb-3">
                                                <label for="validationCustom02">Mobile number</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter product description" required name="prod_desc" value="<?php echo  $row['mobile']; ?>">
												
                                            </div>
                                        </div>
                                        <div class="row">
											<?php
												
												$sql3 = "SELECT * FROM tbl_seller_info WHERE user_id='$seller_id'";
												$result3=mysqli_query($conn,$sql3);
												$row3=$result3->fetch_assoc();
											?>
										
                                            <div class="col-md-4 mb-3">
                                                <label for="validationCustom03">Shop name</label>
                                                <input type="text" class="form-control" id="validationCustom03" placeholder="Enter product price" required name="prod_rate" value="<?php echo  $row3['shop_name']; ?>">
                                                
                                            </div>
                                            <div class="col-md-8 mb-3">
                                                <label for="validationCustom04">Shop address</label>
                                                <input type="text" class="form-control" id="validationCustom04" placeholder="Enter product quantity" required name="prod_quan" value="<?php echo  $row3['shop_address']; ?>">
                                                
                                            </div>
                                           
                                        </div>
										

                                        <div class="row">
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom05">Tax type</label>
												
												<div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input" id="tax_type1" name="tax_type" required value="0" <?php if($row['is_seller']==1){echo 'checked';} ?> >
													<label class="custom-control-label" for="tax_type1">Buyer</label>
												  </div>
												  <div class="custom-control custom-radio mb-3">
													<input type="radio" class="custom-control-input" id="tax_type2" name="tax_type" required value="1" <?php if($row['is_seller']==2){echo 'checked';} ?> >
													<label class="custom-control-label" for="tax_type2">Seller</label>
												  </div>
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom06">Tax category</label>
                                                <div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input" id="tax_cat1" name="tax_cat" required value="0" <?php if($row['isActive']==0){echo 'checked';} ?> >
													<label class="custom-control-label" for="tax_cat1">Inactive</label>
												  </div>
												  <div class="custom-control custom-radio mb-3">
													<input type="radio" class="custom-control-input" id="tax_cat2" name="tax_cat" required value="1" <?php if($row['isActive']==1){echo 'checked';} ?> >
													<label class="custom-control-label" for="tax_cat2">Active</label>
												  </div>
                                                
                                            </div>
                                            <div class="col-md-8 mb-3">
                                                <label for="validationCustom07">GST Tax %</label>
                                                <div class="form-group">
												<select class="custom-select" required name="tax_per">
												  <option value="">Select tax percentage</option>
												  <option value="0" <?php if($row['tax_per']==0){echo 'selected';} ?> >0%</option>
												  <option value="5" <?php if($row['tax_per']==5){echo 'selected';} ?> >5%</option>
												  <option value="12"<?php if($row['tax_per']==12){echo 'selected';} ?>  >12%</option>
												  <option value="18" <?php if($row['tax_per']==18){echo 'selected';} ?> >18%</option>
												  <option value="28" <?php if($row['tax_per']==28){echo 'selected';} ?> >28%</option>
												</select>
												
											  </div>
                                                
                                            </div>
                                        </div>
										
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-3">
												<input type="checkbox" class="custom-control-input" id="isshipping" name="isshipping" <?php if($row['city_shipping']==1){echo 'checked';} ?> value="1" >
												<label class="custom-control-label" for="isshipping">Is shipping charges applicable?</label>
											</div>
                                            
										</div>
										<div class="row">
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom01">Inter city charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="inter_charges" value="<?php echo  $row['city_charges']; ?>" <?php if($row['city_shipping']==1){}else{echo 'disabled="disabled"';} ?>>
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">Out of city charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="out_charges" value="<?php echo  $row['out_charges']; ?>" <?php if($row['city_shipping']==1){}else{echo 'disabled="disabled"';} ?> >
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">State charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="state_charges" value="<?php echo  $row['state_charges']; ?>" <?php if($row['city_shipping']==1){}else{echo 'disabled="disabled"';} ?> >
                                            </div>
											
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Buying points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="buy_point" value="<?php echo  $row['buy_point']; ?>">
                                            </div>
											<div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Refer points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="refer_point" value="<?php echo  $row['refer_point']; ?>">
                                            </div>
                                        </div>
										
                                        <input type="submit" name="submit" value="Add product" class="btn btn-primary">
										
									<?php } ?>
                                    </form>
                                    <script>
                                        ! function() {
                                            "use strict";
                                            window.addEventListener("load", function() {
                                                var e = document.getElementById("needs-validation");
                                                e.addEventListener("submit", function(t) {
                                                    !1 === e.checkValidity() && (t.preventDefault(), t.stopPropagation()), e.classList.add("was-validated")
                                                }, !1)
                                            }, !1)
                                        }()
                                    </script>
									
                                </div>
                            </div>
                        </div>
						</div>
					
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright &copy; <?php echo date('Y',time());?> ISEBY  All rights reserved.</span></footer>
        </div>
    </div>
	
	
	
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	
    <script>
    $('#isshipping').change(function () {
    var checked = this.checked;
    console.log(checked);
    $('.check_enable').each(function () {
        $(this).prop('disabled', !checked);
    });
});
    </script>
</body>


</html>