<?php
    ob_start();
	include('config.php');
    include('session.php');
	
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Settings</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="/">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISBEY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
                    
					<li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Products</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="sellers.php"><span class="icon-holder"><i class="c-blue-500 ti-user"></i> </span><span class="title">Sellers</span></a></li>
                    
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-purple-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
						<li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> Super Admin </a></li>
                        
                    </ul>
                    <ul class="nav-right">
                        
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20 " >
                                    <h4 class="c-grey-900 mB-20">Settings</h4>
									<hr>
									<div id="accordion">
									  <div class="card">
										<div class="card-header" id="headingOne" style="background:#fafafa;">
										  <h5 class="mb-0">
											<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="text-decoration:none;">
											  Seller Information 
											</button>
										  </h5>
										</div>

										<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
										  <div class="card-body">
                                          <form method="post">
											<div class="row">
												<div class="col-md-3 mb-3">
													<label for="validationCustom01">Name</label>
													<input type="text" class="form-control" id="validationCustom01" placeholder="Enter name" required name="name" value="<?php echo $name;?>">
													
												</div>
												<div class="col-md-4 mb-3">
													<label for="validationCustom02">Email</label>
													<input type="email" class="form-control" id="validationCustom02" placeholder="Enter email address" required name="email" value="<?php echo $email;?>">
													
												</div>
												<div class="col-md-3 mb-3">
													<label for="validationCustom03">Mobile number</label>
													<input type="text" class="form-control" id="validationCustom03" placeholder="Enter mobile number" required name="mobile" value="<?php echo $mobile;?>">
													
												</div>
												<div class="col-md-2 mb-3">
													<label for="validationCustom02">Update information</label>
													<input type="submit" name="update" value="Update information" class="form-control btn btn-dark">
													
												</div>
											</div>
											
											
												
												
                                            </form>


                                            <?php 
                                                    if(isset($_POST['update']))
                                                    {
                                                        $name=mysqli_real_escape_string($conn,$_POST['name']);
                                                        $email=mysqli_real_escape_string($conn,$_POST['email']);
                                                        $mobile=mysqli_real_escape_string($conn,$_POST['mobile']);
                                                        
                                                        
                                                        $sql1 = "update superadmin set name='$name', email='$email', phonenumber='$mobile' where id='$id'";
                                                        $result1=mysqli_query($conn,$sql1);

                                                        

                                                        if($result1)
                                                        {
                                                            echo '<script language="javascript">';
															echo 'alert("Information updated successfully"); location.href="'.$_SERVER['HTTP_REFERER'].'"';
															echo '</script>';
                                                        }
                                                        else 
                                                        {
                                                            echo "".mysqli_error($conn);
                                                        }
                                                        
                                                    }
                                                ?>


											
											
											
										  </div>
										</div>
									  </div>
									  
									  
									  <div class="card">
										<div class="card-header" id="headingThree" style="background:#fafafa;">
										  <h5 class="mb-0">
											<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="text-decoration:none;">
											  Change password
											</button>
										  </h5>
										</div>
										<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
										  <div class="card-body">
                                          <form method="post">
                                          <div class="row">
                                            
												<div class="col-md-3 mb-3">
													<label for="validationCustom06">Current password</label>
													<input type="password" class="form-control" id="validationCustom06" placeholder="Enter current password" required name="old_password" >
													
												</div>
												<div class="col-md-3 mb-3">
													<label for="validationCustom07">New password</label>
													<input type="password" class="form-control" id="validationCustom08" placeholder="Enter new password" required name="new_password" >
													
												</div>
												<div class="col-md-3 mb-3">
													<label for="validationCustom09">Reenter new password</label>
													<input type="password" class="form-control" id="validationCustom09" placeholder="Re-enter new password" required name="confirm_password" >
													
												</div>
                                                <div class="col-md-3 mb-3">
													<label for="validationCustom02">Update information</label>
													<input type="submit" name="change" value="Change password" class="form-control btn btn-dark">
													
												</div>
                                                </form>
                                                <?php 
                                                    if(isset($_POST['change']))
                                                    {
                                                        $old=mysqli_real_escape_string($conn,$_POST['old_password']);
                                                        $new_passowrd=mysqli_real_escape_string($conn,$_POST['new_password']);
                                                        $confirm_password=mysqli_real_escape_string($conn,$_POST['confirm_password']);
                                                        
                                                        if($new_passowrd!=$confirm_password)
                                                        {
                                                            echo '<script> alert(\'Password does not match\'); </script>';
                                                        }
                                                        else if($old=='' || $new_passowrd=='' || $confirm_password=='')
                                                        {
                                                            echo '<script> alert(\'Please fill all the fields.\'); </script>';
                                                        }
                                                        else
                                                        {
                                                            $sql = "SELECT * FROM superadmin WHERE id = '$id' and password = '$old'";
                                                            $result = mysqli_query($conn,$sql);
                                                            $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
                                                            $count = mysqli_num_rows($result);

                                                            if($count == 1) {
                                                                $sql = "update superadmin set password='$new_passowrd' where id='$id'";
                                                                mysqli_query($conn,$sql);
                                                                header("location: signin.php");
                                                            }else {
                                                                echo '<script> alert(\'Old password does not match.\'); </script>';
                                                            }
                                                        }
                                                    }
                                                ?>
											</div
										  </div>
										</div>
									  </div>
									</div>
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © <?php echo date('Y',time());?> ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
</body>

</html>
<?php ob_flush();?>
<?php
function encrypt_url($string) {
  $key = "MAL_979877"; //key to encrypt and decrypts.
  $result = '';
  $test = "";
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));

     //$test[$char]= ord($char)+ord($keychar);
     $result.=$char;
   }

   return urlencode(base64_encode($result));
}
function moneyFormatIndia($num){

$explrestunits = "" ;
$num=preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des="00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}
?>