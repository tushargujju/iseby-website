<?php
require('u/fpdf.php');

$prod_code=decrypt_url($_GET['id']);

function decrypt_url($string) {
    $key = "MAL_979877"; //key to encrypt and decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)-ord($keychar));
     $result.=$char;
   }
   return $result;
}


$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;

//html PNG location prefix
$PNG_WEB_DIR = 'temp/';

include "phpqrcode/qrlib.php";    

//ofcourse we need rights to create temp dir
if (!file_exists($PNG_TEMP_DIR))
	mkdir($PNG_TEMP_DIR);


	$filename = $PNG_TEMP_DIR."$prod_code.png";

	//processing form input
	//remember to sanitize user input in real-life solution !!!
	$errorCorrectionLevel = 'L';
if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
	$errorCorrectionLevel = $_REQUEST['level'];    

	$matrixPointSize = 8;
if (isset($_REQUEST['size']))
	$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);

	//default data
	//echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
	QRcode::png($prod_code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);


class PDF extends FPDF
{


function Header()
{
if(!empty($_FILES["file"]))
  {
$uploaddir = "temp/test.png";
$nm = $_FILES["file"]["name"];
$random = rand(1,99);
move_uploaded_file($_FILES["file"]["tmp_name"], $uploaddir.$random.$nm);
$this->Image($uploaddir.$random.$nm,10,10,20);
unlink($uploaddir.$random.$nm);
}
$this->SetFont('Arial','B',12);
$this->Ln(1);
}
function Footer()
{
$this->SetY(-15);
$this->SetFont('Arial','I',8);
$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
$this->Cell(0,10,'ISEBY | You see it, You buy it',0,0,'R');
}
function ChapterTitle($num, $label)
{
$this->SetFont('Arial','',12);
$this->SetFillColor(200,220,255);
$this->Cell(0,10,'ISEBY',0,0,'R',true);
$this->Ln(0);
}
function ChapterTitle2($num, $label)
{
$this->SetFont('Arial','',12);
$this->SetFillColor(249,249,249);
$this->Cell(0,6,"$num $label",0,1,'L',true);
$this->Ln(0);
}

function WordWrap(&$text, $maxwidth)
{
    $text = trim($text);
    if ($text==='')
        return 0;
    $space = $this->GetStringWidth(' ');
    $lines = explode("\n", $text);
    $text = '';
    $count = 0;

    foreach ($lines as $line)
    {
        $words = preg_split('/ +/', $line);
        $width = 0;

        foreach ($words as $word)
        {
            $wordwidth = $this->GetStringWidth($word);
            if ($wordwidth > $maxwidth)
            {
                // Word is too long, we cut it
                for($i=0; $i<strlen($word); $i++)
                {
                    $wordwidth = $this->GetStringWidth(substr($word, $i, 1));
                    if($width + $wordwidth <= $maxwidth)
                    {
                        $width += $wordwidth;
                        $text .= substr($word, $i, 1);
                    }
                    else
                    {
                        $width = $wordwidth;
                        $text = rtrim($text)."\n".substr($word, $i, 1);
                        $count++;
                    }
                }
            }
            elseif($width + $wordwidth <= $maxwidth)
            {
                $width += $wordwidth + $space;
                $text .= $word.' ';
            }
            else
            {
                $width = $wordwidth + $space;
                $text = rtrim($text)."\n".$word.' ';
                $count++;
            }
        }
        $text = rtrim($text)."\n";
        $count++;
    }
    $text = rtrim($text);
    return $count;
}



}


$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->AddPage();
$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(32);

$image1 = "temp/$prod_code.png";
for($i=0;$i<6;$i++)
{
	$pdf->Cell(35,4,'ISBEY','',0,'C',0);
	$pdf->Cell(37,4,'ISBEY','',0,'C',0);
	$pdf->Cell(37,4,'ISBEY','',0,'C',0);
	$pdf->Cell(36,4,'ISBEY','',0,'C',0);
	$pdf->Cell(37,4,'ISBEY','',1,'C',0);
	
	$pdf->Cell( 37, 32, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
	$pdf->Cell( 37, 32, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
	$pdf->Cell( 37, 32, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
	$pdf->Cell( 37, 32, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
	$pdf->Cell( 37, 32, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 1, 'L', false );
	
	$pdf->Cell(35,5,$prod_code,'',0,'C',0);
	$pdf->Cell(37,5,$prod_code,'',0,'C',0);
	$pdf->Cell(37,5,$prod_code,'',0,'C',0);
	$pdf->Cell(36,5,$prod_code,'',0,'C',0);
	$pdf->Cell(37,5,$prod_code,'',1,'C',0);
	
	$pdf->Cell(190,3,'','',1,'C',0);
}

$filename="pdf/".$prod_code.".pdf";
$pdf->Output($filename,'I');

?>