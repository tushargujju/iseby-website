<?php
	include('config.php');
	include('session.php');

	global $random;
	$random = time();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Stock management</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }

        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }

        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }

        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }

        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="/">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISEBY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">

					<li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Products</span></a></li>
					<li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="sellers.php"><span class="icon-holder"><i class="c-blue-500 ti-user"></i> </span><span class="title">Sellers</span></a></li>
                    
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-purple-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>

                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
						<li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> Super Admin </a></li>

                    </ul>
                    <ul class="nav-right">


                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20 " >
																		
                                    <h4 class="c-grey-900 mB-20">Total products</h4>

                                    <table id="dataTable" class="table table-hover " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Seller #</th>
                                                <th>Product name</th>
												<th>Product code</th>
                                                <th>Quantity</th>
                                                <th>MRP</th>
                                                <th>Discount</th>
												<th>Avaibility</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                                <?php
													$sql = "SELECT * FROM tbl_products";
													$result=mysqli_query($conn,$sql);
													$i=1;
													while($row=$result->fetch_assoc())
													{
														$quan=$row['quantity'];
														if($quan>50)
														{
															$status ='<span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="Greater than 50 quantity available">Available</span>';
														}
														else if($quan<=50 && $quan>0)
														{
															$status ='<span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="Less than 50 quantity available">Running low</span>';
														}
														else if($quan==0)
														{
															$status ='<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill" data-toggle="tooltip" data-placement="top" title="0 quantity available">Out of stock</span>';
														}
												?>
													<tr>
														<td><?php echo $i; ?></td>
														<td>
															<?php
																$seller_id=$row['seller_id'];

																$sql2 = "SELECT * FROM tbl_users WHERE id='$seller_id'";
																$result2=mysqli_query($conn,$sql2);
																$row2=$result2->fetch_assoc();

																$sql3 = "SELECT * FROM tbl_seller_info WHERE user_id='$seller_id'";
																$result3=mysqli_query($conn,$sql3);
																$row3=$result3->fetch_assoc();
																?>
																<span tabindex="0" data-toggle="popover" data-trigger="hover" title="<?php echo ucwords($row2['name']);?>" data-html="true" data-content="<?php echo 'Shop name : '.ucwords($row3['shop_name']).'<br/>Shop address :'.ucwords($row3['shop_address']).'<br/>';?>"><?php echo $seller_id?></span>
														</td>
														<td><?php echo $row['name']; ?></td>
														<td><?php echo $row['product_code']; ?></td>
														<td><?php echo $row['quantity']; ?></td>
														<td><?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row['price'],2,'.','')); ?></td>
														<td><?php echo $row['discount_per']."%"; ?></td>
														<td><?php echo $status;
														?></td>

														<td>
														<div class="peers mR-15">

															<div class="peer">
																<a href="view_product.php?id=<?php echo encrypt_url($row['id']);?>&s_id=<?php echo encrypt_url($row['seller_id']);?>" class="td-n c-purple-500 cH-blue-500 fsz-md p-5"><i class="ti-eye"></i></a>
															</div>
															<?php
																	if($row['isActive']==0)
																	{
															?>
																	<div class="peer">
																			<a href="unblock_product.php?id=<?php echo encrypt_url($row['id']);?>&s_id=<?php echo encrypt_url($row['seller_id']);?>" class="td-n c-green-500 cH-green-500 fsz-md p-5" data-toggle="tooltip" data-placement="top" title="Unblock"><i class="ti-unlock"></i></a>
																	</div>
															<?php
														}elseif($row['isActive']==1)
																	{
															?>
																	<div class="peer">
																			<a href="block_product.php?id=<?php echo encrypt_url($row['id']);?>&s_id=<?php echo encrypt_url($row['seller_id']);?>" class="td-n c-orange-500 cH-orange-500 fsz-md p-5" data-toggle="tooltip" data-placement="top" title="Block"><i class="ti-na"></i></a>
																	</div>
															<?php
																	}
															?>

														</div>
														<?php //echo '<a href="delete_from_stock.php?id='. $row['id'].'" class="delete" style="text-decoration:none;" onClick="return confirm(\'Please confirm deletion\');"><i class="c-red-500 ti-trash xlarge"></i></a>';?>
														</td>
													</tr>
												<?php
														$i++;
													}
												?>



                                        </tbody>
                                    </table>
								</br></br>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright &copy; <?php echo date('Y',time());?> ISBEY - All rights reserved.</span></footer>
        </div>
    </div>
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
</body>

</html>

<?php
function encrypt_url($string) {
  $key = "MAL_979877"; //key to encrypt and decrypts.
  $result = '';
  $test = "";
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));

     //$test[$char]= ord($char)+ord($keychar);
     $result.=$char;
   }

   return urlencode(base64_encode($result));
}
function moneyFormatIndia($num){

$explrestunits = "" ;
$num=preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des="00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}
?>
