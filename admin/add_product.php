<?php
ob_start();
ini_set("log_errors", 1);
ini_set("error_log", "tmp/php-error.log");

include('config.php');    
include('session.php');

require 'src/Cloudinary.php';
require 'src/Uploader.php';
require 'src/Api.php';




	date_default_timezone_set('Asia/Kolkata');
	$today = date('Y-m-d', time());
	
    if(!$_SESSION['login_user'])
    {
		header('location:login.php');
    }
	
	global $u;
	$u = time();
	
	
	global $error;

	global $number;
	
	if(isset($_POST['submit'])) {
	
	  
	  $prod_name = mysqli_real_escape_string($conn,$_POST['prod_name']);
	  $prod_desc = mysqli_real_escape_string($conn,$_POST['prod_desc']);
    
	  $prod_rate=mysqli_real_escape_string($conn,$_POST['prod_rate']);
	  $prod_quan=mysqli_real_escape_string($conn,$_POST['prod_quan']);
	  $discount_per=mysqli_real_escape_string($conn,$_POST['discount_per']);
	  
	  $category=mysqli_real_escape_string($conn,$_POST['category']);
	  $sub_category=mysqli_real_escape_string($conn,$_POST['sub_category']);
	  
	  $isshipping=mysqli_real_escape_string($conn,$_POST['isshipping']);
		
	  $inter_charges=mysqli_real_escape_string($conn,$_POST['inter_charges']);
	  $out_charges=mysqli_real_escape_string($conn,$_POST['out_charges']);
	  $state_charges=mysqli_real_escape_string($conn,$_POST['state_charges']);
	  
	  $buy_point=mysqli_real_escape_string($conn,$_POST['buy_point']);
	  $refer_point=mysqli_real_escape_string($conn,$_POST['refer_point']);
     
	 	  
	   if (empty($prod_name) || empty($prod_desc) || empty($prod_rate)  || empty($discount_per) || empty($category) || empty($sub_category)||empty($buy_point)||empty($refer_point) )
	  {
		  
		  
		  
	  }
	  else 
	  {
		  $prod_no=genRandomString(6);
		  
		  $barcode_color="#000000";
		  $sql = "insert into tbl_products 
		  (product_code, name, description, seller_id, quantity, price, discount_per, barcode_color, category_id, sub_category, created_at, modified_at, isActive, sgstid, city_shipping, city_charges, out_shipping, out_charges, state_shipping, state_charges, buy_point, refer_point) 
		  values 
		  ('$prod_no','$prod_name','$prod_desc','$id','$prod_quan', '$prod_rate', '$discount_per', '$barcode_color', '$category', '$sub_category', '$u', '$u', 1, 1, '$isshipping', '$inter_charges', '$isshipping', '$out_charges', '$isshipping', '$state_charges', '$buy_point', '$refer_point')";
		  
		  
		  $result=mysqli_query($conn,$sql);
		  $last_id = mysqli_insert_id($conn);
		  
		  if(!$result)
		  {
			echo "Error:".mysqli_error($conn);
		  }
		  else
		  {
			  
			  
			  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
			  //html PNG location prefix
			  $PNG_WEB_DIR = 'temp/';

			  include "phpqrcode/qrlib.php";    
				
			  //ofcourse we need rights to create temp dir
			  if (!file_exists($PNG_TEMP_DIR))
				mkdir($PNG_TEMP_DIR);
				
				
			  $filename = $PNG_TEMP_DIR."$prod_no.png";
				
			  //processing form input
			  //remember to sanitize user input in real-life solution !!!
			  $errorCorrectionLevel = 'L';
			  if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
				$errorCorrectionLevel = $_REQUEST['level'];    

		      $matrixPointSize = 8;
			  if (isset($_REQUEST['size']))
				$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
				
				//default data
				//echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
				QRcode::png($prod_no, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
			
			\Cloudinary::config(array( 
			  "cloud_name" => "isbey-sample", 
			  "api_key" => "979539496732217", 
			  "api_secret" => "IWowFCLgR6ZCODUldPc_3-sn04A" 
			));

			
			$qr_url = \Cloudinary\Uploader::upload($filename, $options = array("quality"=>"auto:low"));
			$qr_url= $qr_url['secure_url'];
			
			$sql1 = "insert into tbl_media (file_name, product_id, type, created_at, modified_at) values ('$qr_url','$last_id','2','$u','$u')";
			$result1=mysqli_query($conn,$sql1);
			
			foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
				$temp = $_FILES["files"]["tmp_name"][$key];
				$name = $_FILES["files"]["name"][$key];
				 
				if(empty($temp))
				{
					break;
				}
				
				$prod_url = \Cloudinary\Uploader::upload($temp, $options = array("quality"=>"auto:low"));
				$prod_url= $prod_url['secure_url'];
				
				$sql2 = "insert into tbl_media (file_name, product_id, type, created_at, modified_at) values ('$prod_url','$last_id','1','$u','$u')";
				$result2=mysqli_query($conn,$sql2);
			}
			
			//$error="Product added successfully.";
			echo '<script language="javascript">';
			echo 'alert("Product added Successfully"); location.href="add_product.php"';
			echo '</script>';
		  }

	  }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Add product</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="index.html">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISEBY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
				
                    <li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="add_product.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Add product</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-orange-500 ti-list"></i> </span><span class="title">Stock Management</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-receipt"></i> </span><span class="title">Transaction</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-blue-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="reports.php"><span class="icon-holder"><i class="c-teal-500 ti-stats-up"></i> </span><span class="title">Reports</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-orange-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                        <li class="search-box"><a class="search-toggle no-pdd-right" href="javascript:void(0);"><i class="search-icon ti-search pdd-right-10"></i> <i class="search-icon-close ti-close pdd-right-10"></i></a></li>
                        <li class="search-input">
                            <input class="form-control" type="text" placeholder="Search...">
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="notifications dropdown"><span class="counter bgc-red">3</span> <a href="" class="dropdown-toggle no-after" data-toggle="dropdown"><i class="ti-bell"></i></a>
                            <ul class="dropdown-menu">
                                <li class="pX-20 pY-15 bdB"><i class="ti-bell pR-10"></i> <span class="fsz-sm fw-600 c-grey-900">Notifications</span></li>
                                <li>
                                    <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                        <li>
                                            <a href="" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                                <div class="peer mR-15"><img class="w-3r bdrs-50p" src="https://randomuser.me/api/portraits/men/1.jpg" alt=""></div>
                                                <div class="peer peer-greed"><span><span class="fw-500">John Doe</span> <span class="c-grey-600">liked your <span class="text-dark">post</span></span>
                                                    </span>
                                                    <p class="m-0"><small class="fsz-xs">5 mins ago</small></p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                                <div class="peer mR-15"><img class="w-3r bdrs-50p" src="https://randomuser.me/api/portraits/men/2.jpg" alt=""></div>
                                                <div class="peer peer-greed"><span><span class="fw-500">Moo Doe</span> <span class="c-grey-600">liked your <span class="text-dark">cover image</span></span>
                                                    </span>
                                                    <p class="m-0"><small class="fsz-xs">7 mins ago</small></p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                                <div class="peer mR-15"><img class="w-3r bdrs-50p" src="https://randomuser.me/api/portraits/men/3.jpg" alt=""></div>
                                                <div class="peer peer-greed"><span><span class="fw-500">Lee Doe</span> <span class="c-grey-600">commented on your <span class="text-dark">video</span></span>
                                                    </span>
                                                    <p class="m-0"><small class="fsz-xs">10 mins ago</small></p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pX-20 pY-15 ta-c bdT"><span><a href="" class="c-grey-600 cH-blue fsz-sm td-n">View All Notifications <i class="ti-angle-right fsz-xs mL-10"></i></a></span></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="https://randomuser.me/api/portraits/men/10.jpg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900">John Doe</span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                <li><a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-user mR-10"></i> <span>Profile</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    
						<div class="row gap-20 masonry pos-r" >
							<div class="masonry-sizer col-md-6"></div>
							<div class="masonry-item col-md-12">
                            <div class="bgc-white p-20 bd">
                                <h4 class="c-grey-900">Add product </h4>
                                <div class="mT-30">
                                    <form class="container" id="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom01">Product name</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter product name" required name="prod_name">
												
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom02">Description</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter product description" required name="prod_desc">
												
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom03">Price</label>
                                                <input type="text" class="form-control" id="validationCustom03" placeholder="Enter product price" required name="prod_rate">
                                                
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom04">Quantity</label>
                                                <input type="text" class="form-control" id="validationCustom04" placeholder="Enter product quantity" required name="prod_quan">
                                                
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom05">Discount %</label>
                                                <input type="text" class="form-control" id="validationCustom05" placeholder="Enter discount percentage" required name="discount_per">
                                                
                                            </div>
                                        </div>
										<div class="row">
											<div class="form-group col-md-6">
                                                <label for="inputState">Categories</label>
                                                <select id="inputState" class="form-control" name="category">
                                                    <?php
														$sql = "SELECT * FROM tbl_categories";
														$result=mysqli_query($conn,$sql);
														while($row=$result->fetch_assoc())
														{
													?>	
														<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
														<?php
														}
														?>	
                                                </select>
                                            </div>
											<div class="form-group col-md-6">
                                                <label for="inputState">Subcategory</label>
                                                <select id="inputState" class="form-control" name="sub_category">
                                                    <?php
														$sql = "SELECT * FROM tbl_sub_categories";
														$result=mysqli_query($conn,$sql);
														while($row=$result->fetch_assoc())
														{
													?>	
														<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
														<?php
														}
														?>	
                                                </select>
                                            </div>
										</div>
										
										<div class="form-group">
                                            <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="isshipping" value="1"> Is shipping charges applicable?</label>
                                            </div>
											</div>
										<div class="row">
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom01">Inter city charges</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter charges" required name="inter_charges">
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">Out of city charges</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter charges" required name="out_charges">
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">State charges</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter charges" required name="state_charges">
                                            </div>
											
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Buying points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="buy_point">
                                            </div>
											<div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Refer points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="refer_point">
                                            </div>
                                        </div>
										<div class="row">
											<div class="col-md-6 mb-3">
												<output id="list"></output>
												</div>
											<div class="col-md-6 mb-3">
												<input type="file" id="files" name="files[]" multiple="multiple" accept="image/*" data-toggle="tooltip" data-placement="top" title="Please select maximum 6 images" required />
												
											</div>
										</div>
                                        <input type="submit" name="submit" value="Add product" class="btn btn-primary">
                                    </form>
                                    <script>
                                        ! function() {
                                            "use strict";
                                            window.addEventListener("load", function() {
                                                var e = document.getElementById("needs-validation");
                                                e.addEventListener("submit", function(t) {
                                                    !1 === e.checkValidity() && (t.preventDefault(), t.stopPropagation()), e.classList.add("was-validated")
                                                }, !1)
                                            }, !1)
                                        }()
                                    </script>
									
                                </div>
                            </div>
                        </div>
						</div>
					
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © 2017 ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
	
	
	
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	
</body>

<script>
	function handleFileSelect(evt) {
    var files = evt.target.files;

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = 
          [
            '<img style="height: 30px; border: 1px solid #000; margin: 5px" src="', 
            e.target.result,
            '" title="', escape(theFile.name), 
            '"/>'
          ].join('');
          
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  document.getElementById('files').addEventListener('change', handleFileSelect, false);
	
</script>
</html>

<?php

function genRandomString($length) {
   $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $string ='IS';    
   $str_len=strlen($characters)-1;
   echo $str_len.'</br>';
   for ($p = 0; $p < $length; $p++) {
	  $string .= $characters[mt_rand(0, $str_len)];
	}
	include('config.php');
	$sql = "SELECT * FROM tbl_products WHERE product_code = '$string'";
	$result = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$count = mysqli_num_rows($result);
	
	if($count == 1) 
	{
		genRandomString($length);
	}else 
	{
		return $string;
	}
  }
?>