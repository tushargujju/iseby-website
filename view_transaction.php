<?php
	include('config.php');
	include('session.php');
	ob_start();
	
	date_default_timezone_set('Asia/Kolkata');
    $today = date('Y-m-d', time());

    if($expiry_date < $today)
	{
		header('location:401.php');
	}
	
	$trans_id=decrypt_url($_GET['id']);
	$s_id=decrypt_url($_GET['s_id']);
	
	function decrypt_url($string) {
		$key = "MAL_979877"; //key to encrypt and decrypts.
		$result = '';
		$string = base64_decode(urldecode($string));
	   for($i=0; $i<strlen($string); $i++) {
		 $char = substr($string, $i, 1);
		 $keychar = substr($key, ($i % strlen($key))-1, 1);
		 $char = chr(ord($char)-ord($keychar));
		 $result.=$char;
	   }
	   return $result;
	}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>View Transaction</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="/">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">Adminator</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
                    
					<li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="add_product.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Add product</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-orange-500 ti-list"></i> </span><span class="title">Stock Management</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-receipt"></i> </span><span class="title">Transaction</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-blue-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="reports.php"><span class="icon-holder"><i class="c-teal-500 ti-stats-up"></i> </span><span class="title">Reports</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-orange-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="forms.html"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
						<li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> <?php echo $shop_name;?></a></li>
                        
                    </ul>
                    <ul class="nav-right">
                        
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20 " >
                                    <h4 class="c-grey-900 mB-20">Transaction Summary</h4>
										<form method="POST">
											<div class="row">
												<?php
													$sql1 = "SELECT * FROM tbl_transaction WHERE seller_id='$s_id' AND id='$trans_id'";
													$result1=mysqli_query($conn,$sql1);
													while($row1=$result1->fetch_assoc())
													{
														
												?>
												<div class="col-md-4 mb-3">
													<h6>Transaction Date : <?php $date1=date_create($row1['trans_date']);echo date_format($date1,'d/m/Y h:i:s A'); ?></h6>
													<h6>Transaction Id : <?php echo $trans_id; ?></h6>
													
												</div>
												<div class="col-md-4 mb-3">
													<h6>
													<h6>
													<?php
														$buyer_id=$row1['buyer_id'];
														$sql2 = "SELECT * FROM tbl_users WHERE id='$buyer_id'";
														$result2=mysqli_query($conn,$sql2);
														while($row2=$result2->fetch_assoc())
														{
															echo "Buyer Name : ".ucwords($row2['name']);
															?>
													</h6>
													<h6>
													<?php
															
														echo "Mobile no. : ".$row2['mobile']."</h6>";
														echo '<h6>Total buy points earned : '.$row1['buy_points']."</h6>";
														
														}
													?></h6>
												</div>
												<div class="col-md-4 mb-3">
													
													<h6>Subtotal&nbsp;&nbsp;:&nbsp;<?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row1['sub_total'],2,'.','')); ?></h6>
													<h6>Discount&nbsp;:&nbsp;<?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row1['discount'],2,'.','')); ?></h6><hr>
													<h5>Grand total : <?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row1['trans_amount'],2,'.','')); ?></h5>
												</div>
												<?php
													}
												?>
											</div>
											
										</form></br>

                                    <table class="table table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product name</th>
												<th>MRP</th>
                                                <th>Quantity</th>
                                                <th>Discount %</th>
												<th>Total</th>
												<th>Buy Points</th>
                                            </tr>
                                        </thead>
										<tbody>	
										<?php
											$sql = "SELECT * FROM tbl_transaction_products WHERE seller_id='$s_id' AND trans_id='$trans_id'";
											$result=mysqli_query($conn,$sql);
											$i=1;
											while($row=$result->fetch_assoc())
											{
												
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $row['prod_name']; ?></td>
											<td><?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row['price'],2,'.','')); ?></td>
											<td><?php echo $row['quantity']; ?></td>
											<td><?php echo $row['discount_per']."%"; ?></td>
											<td><?php echo "&#8377 ".moneyFormatIndia(number_format((float)$row['total'],2,'.','')); ?></td>
											<td><?php echo $row['buy_points']; ?></td>
										</tr>
										<?php
											$i++;
											}
										?>
		                                </tbody>
                                    </table>
									
								</div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © <?php echo date('Y',time());?> ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
	<script>
    $(document).ready(function () {
        $('#prod_code').typeahead({
            source: function (query, result) {
                $.ajax({
                    url: "search_product.php",
					data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						result($.map(data, function (item) {
							return item;
                        }));
                    }
                });
            }
        });
    });
</script>
<style> .typeahead {
    
    
    
    max-width: 100%;
    min-width: 95%;
    
    color: #000;
}

.tt-menu {
    width: 300px;
}

ul.typeahead {
    margin: 0px;
    padding: 10px 0px;
}

ul.typeahead.dropdown-menu li a {
    padding: 10px !important;
    border-bottom: #CCC 1px solid;
    color: #333;
}

ul.typeahead.dropdown-menu li:last-child a {
    border-bottom: 0px !important;
}



.dropdown-menu>.active>a,
.dropdown-menu>.active>a:focus,
.dropdown-menu>.active>a:hover {
    text-decoration: none;
    background-color: #dcdcdc;
    outline: 0;
	
	
}

</style>
</body>

</html>

<?php

function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    
	return ($Rupees ? $Rupees . 'Rupees' : '') . $paise ;
}


function encrypt_url($string) {
  $key = "MAL_979877"; //key to encrypt and decrypts.
  $result = '';
  $test = "";
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));

     //$test[$char]= ord($char)+ord($keychar);
     $result.=$char;
   }

   return urlencode(base64_encode($result));
}
function moneyFormatIndia($num){

$explrestunits = "" ;
$num=preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des="00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}
?>