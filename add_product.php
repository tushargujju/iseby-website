<?php
ob_start();
ini_set("log_errors", 1);
ini_set("error_log", "tmp/php-error.log");

include('config.php');    
include('session.php');

date_default_timezone_set('Asia/Kolkata');
    $today = date('Y-m-d', time());

    if($expiry_date < $today)
	{
		header('location:401.php');
	}

require 'src/Cloudinary.php';
require 'src/Uploader.php';
require 'src/Api.php';




	date_default_timezone_set('Asia/Kolkata');
	$today = date('Y-m-d', time());
	
    if(!$_SESSION['login_user'])
    {
		header('location:login.php');
    }
	
	global $u;
	$u = time();
	
	
	global $error;

	global $number;
	
	if(isset($_POST['submit'])) {
	
	  
	  $prod_name = mysqli_real_escape_string($conn,$_POST['prod_name']);
	  $prod_desc = mysqli_real_escape_string($conn,$_POST['prod_desc']);
    
	  $prod_rate=mysqli_real_escape_string($conn,$_POST['prod_rate']);
	  $prod_quan=mysqli_real_escape_string($conn,$_POST['prod_quan']);
	  $discount_per=mysqli_real_escape_string($conn,$_POST['discount_per']);
	  
	  $category=mysqli_real_escape_string($conn,$_POST['category']);
	  $sub_category=mysqli_real_escape_string($conn,$_POST['sub_category']);
	  
	  $tax_type=mysqli_real_escape_string($conn,$_POST['tax_type']);
	  $tax_cat=mysqli_real_escape_string($conn,$_POST['tax_cat']);
	  $tax_per=mysqli_real_escape_string($conn,$_POST['tax_per']);
	  
	  $isshipping=mysqli_real_escape_string($conn,$_POST['isshipping']);
		
	  if(isset($_POST['isshipping']))
	  {
		  $inter_charges=mysqli_real_escape_string($conn,$_POST['inter_charges']);
		  $out_charges=mysqli_real_escape_string($conn,$_POST['out_charges']);
		  $state_charges=mysqli_real_escape_string($conn,$_POST['state_charges']);
	  }
	  else
	  {
		  $inter_charges=0;
		  $out_charges=0;
		  $state_charges=0;
	  }
	  
	  $buy_point=mysqli_real_escape_string($conn,$_POST['buy_point']);
	  $refer_point=mysqli_real_escape_string($conn,$_POST['refer_point']);
     
	 	  
	   if (empty($prod_name) || empty($prod_desc) || empty($prod_rate)  || empty($discount_per) || empty($category) || empty($sub_category)||empty($buy_point)||empty($refer_point) )
	  {
		  
		  
		  
	  }
	  else 
	  {
		  $prod_no=genRandomString(6);
		  
		  $barcode_color="#000000";
		  $sql = "insert into tbl_products 
		  (product_code, name, description, seller_id, quantity, price, discount_per, barcode_color, category_id, sub_category, created_at, modified_at, isActive, sgstid, city_shipping, city_charges, out_shipping, out_charges, state_shipping, state_charges, buy_point, refer_point, tax_type, tax_cat, tax_per) 
		  values 
		  ('$prod_no','$prod_name','$prod_desc','$id','$prod_quan', '$prod_rate', '$discount_per', '$barcode_color', '$category', '$sub_category', '$u', '$u', 1, 1, '$isshipping', '$inter_charges', '$isshipping', '$out_charges', '$isshipping', '$state_charges', '$buy_point', '$refer_point', '$tax_type', '$tax_cat', '$tax_per')";
		  
		  
		  $result=mysqli_query($conn,$sql);
		  $last_id = mysqli_insert_id($conn);
		  
		  if(!$result)
		  {
			echo "Error:".mysqli_error($conn);
		  }
		  else
		  {
			  
			  
			  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
			  //html PNG location prefix
			  $PNG_WEB_DIR = 'temp/';

			  include "phpqrcode/qrlib.php";    
				
			  //ofcourse we need rights to create temp dir
			  if (!file_exists($PNG_TEMP_DIR))
				mkdir($PNG_TEMP_DIR);
				
				
			  $filename = $PNG_TEMP_DIR."$prod_no.png";
				
			  //processing form input
			  //remember to sanitize user input in real-life solution !!!
			  $errorCorrectionLevel = 'L';
			  if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
				$errorCorrectionLevel = $_REQUEST['level'];    

		      $matrixPointSize = 8;
			  if (isset($_REQUEST['size']))
				$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
				
				//default data
				//echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
				QRcode::png($prod_no, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
			
			\Cloudinary::config(array( 
			  "cloud_name" => "isbey-sample", 
			  "api_key" => "979539496732217", 
			  "api_secret" => "IWowFCLgR6ZCODUldPc_3-sn04A" 
			));

			
			$qr_url = \Cloudinary\Uploader::upload($filename, $options = array("quality"=>"auto:low"));
			$qr_url= $qr_url['secure_url'];
			
			$sql1 = "insert into tbl_media (file_name, product_id, type, created_at, modified_at) values ('$qr_url','$last_id','2','$u','$u')";
			$result1=mysqli_query($conn,$sql1);
			
			foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
				$temp = $_FILES["files"]["tmp_name"][$key];
				$name = $_FILES["files"]["name"][$key];
				 
				if(empty($temp))
				{
					break;
				}
				
				$prod_url = \Cloudinary\Uploader::upload($temp, $options = array("quality"=>"auto:low"));
				$prod_url= $prod_url['secure_url'];
				
				$sql2 = "insert into tbl_media (file_name, product_id, type, created_at, modified_at) values ('$prod_url','$last_id','1','$u','$u')";
				$result2=mysqli_query($conn,$sql2);
			}
			
			//$error="Product added successfully.";
			echo '<script language="javascript">';
			echo 'alert("Product added Successfully"); location.href="add_product.php"';
			echo '</script>';
		  }

	  }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Add product</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }
        
        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }
        
        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }
        
        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }
        
        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script>
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 300);
        });
    </script>
    <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                    <div class="peers ai-c fxw-nw">
                        <div class="peer peer-greed">
                            <a class="sidebar-link td-n" href="index.html">
                                <div class="peers ai-c fxw-nw">
                                    <div class="peer">
                                        <div class="logo"><img src="assets/static/images/logo1.png" alt=""></div>
                                    </div>
                                    <div class="peer peer-greed">
                                        <h5 class="lh-1 mB-0 logo-text">ISEBY</h5></div>
                                </div>
                            </a>
                        </div>
                        <div class="peer">
                            <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu scrollable pos-r">
				
                    <li class="nav-item mT-30 active"><a class="sidebar-link" href="index.php"><span class="icon-holder"><i class="c-indigo-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="add_product.php"><span class="icon-holder"><i class="c-teal-500 ti-shopping-cart"></i> </span><span class="title">Add product</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="stock.php"><span class="icon-holder"><i class="c-orange-500 ti-list"></i> </span><span class="title">Stock Management</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction.php"><span class="icon-holder"><i class="c-deep-orange-500 ti-receipt"></i> </span><span class="title">Transaction</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="transaction_history.php"><span class="icon-holder"><i class="c-blue-500 ti-layout-list-thumb"></i> </span><span class="title">Transaction History</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="reports.php"><span class="icon-holder"><i class="c-teal-500 ti-stats-up"></i> </span><span class="title">Reports</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="setting.php"><span class="icon-holder"><i class="c-orange-500 ti-settings"></i> </span><span class="title">Setting</span></a></li>
                    <li class="nav-item"><a class="sidebar-link" href="logout.php"><span class="icon-holder"><i class="c-red-500 ti-power-off"></i> </span><span class="title">Logout</span></a></li>
					
                </ul>
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                        <li class="search-box"><a class="search-toggle no-pdd-right" style="font-weight:400;font-size:24px;"> <?php echo ucwords($shop_name);?></a></li>
                    </ul>
                    <ul class="nav-right">
                        
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="assets/static/images/user.svg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900"><?php echo ucwords($name);?></span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li><a href="setting.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="logout.php" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    
						<div class="row gap-20 masonry pos-r" >
							<div class="masonry-sizer col-md-6"></div>
							<div class="masonry-item col-md-12">
                            <div class="bgc-white p-20 bd">
                                <h4 class="c-grey-900">Add product </h4>
                                <div class="mT-30">
                                    <form class="container" id="needs-validation" novalidate method="POST" enctype="multipart/form-data">
										<div class="row">
											<div class="form-group col-md-6">
                                                <label for="category">Categories</label>
                                                <select id="category" class="form-control custom-select" name="category" onChange="getState(this.value);" autofocus required>
													<option value="">Select category</option>
                                                    <?php
														$sql = "SELECT * FROM tbl_categories";
														$result=mysqli_query($conn,$sql);
														while($row=$result->fetch_assoc())
														{
													?>	
														<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
														<?php
														}
														?>	
                                                </select>
                                            </div>
											<div class="form-group col-md-6">
                                                <label for="sub_category">Subcategory</label>
                                                <select id="sub_category" class="form-control custom-select" name="sub_category" required>
                                                    
                                                </select>
                                            </div>
										</div>
									
									
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom01">Product name</label>
                                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter product name" required name="prod_name">
												
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom02">Description</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter product description" required name="prod_desc">
												
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom03">Price</label>
                                                <input type="text" class="form-control" id="validationCustom03" placeholder="Enter product price" required name="prod_rate">
                                                
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom04">Quantity</label>
                                                <input type="text" class="form-control" id="validationCustom04" placeholder="Enter product quantity" required name="prod_quan">
                                                
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom05">Discount %</label>
                                                <input type="text" class="form-control" id="validationCustom05" placeholder="Enter discount percentage" required name="discount_per">
                                                
                                            </div>
                                        </div>
										
										
										<div class="row">
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom05">Tax type</label>
												
												<div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input" id="tax_type1" name="tax_type" required value="0">
													<label class="custom-control-label" for="tax_type1">Inclusive Tax</label>
												  </div>
												  <div class="custom-control custom-radio mb-3">
													<input type="radio" class="custom-control-input" id="tax_type2" name="tax_type" required value="1">
													<label class="custom-control-label" for="tax_type2">Exclusive Tax</label>
												  </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom06">Tax category</label>
                                                <div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input" id="tax_cat1" name="tax_cat" required value="0">
													<label class="custom-control-label" for="tax_cat1">CGST + SGST</label>
												  </div>
												  <div class="custom-control custom-radio mb-3">
													<input type="radio" class="custom-control-input" id="tax_cat2" name="tax_cat" required value="1">
													<label class="custom-control-label" for="tax_cat2">IGST</label>
												  </div>
                                                
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom07">GST Tax %</label>
                                                <div class="form-group">
												<select class="custom-select" required name="tax_per">
												  <option value="">Select tax percentage</option>
												  <option value="0">0%</option>
												  <option value="5">5%</option>
												  <option value="12">12%</option>
												  <option value="18">18%</option>
												  <option value="28">28%</option>
												</select>
												
											  </div>
                                                
                                            </div>
                                        </div>
										
										
										
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-3">
												<input type="checkbox" class="custom-control-input" id="isshipping" name="isshipping">
												<label class="custom-control-label" for="isshipping">Is shipping charges applicable?</label>
											</div>
                                            
										</div>
										<div class="row">
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom01">City charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="inter_charges" disabled="disabled">
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">State charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="out_charges" disabled="disabled">
                                            </div>
											<div class="col-md-2 mb-3">
                                                <label for="validationCustom01">National charges</label>
                                                <input type="text" class="form-control check_enable" id="validationCustom01" placeholder="Enter charges" required name="state_charges" disabled="disabled">
                                            </div>
											
                                            <div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Buying points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="buy_point">
                                            </div>
											<div class="col-md-3 mb-3">
                                                <label for="validationCustom02">Refer points</label>
                                                <input type="text" class="form-control" id="validationCustom02" placeholder="Enter points" required name="refer_point">
                                            </div>
                                        </div>
										<div class="row">
											<div class="col-md-6 mb-3">
												<output id="list"></output>
												</div>
											<div class="col-md-6 mb-3">
												<label for="validationCustom04">Prduct images (maximum 6)</label>
												<div class="custom-file">
													<input type="file" id="files" name="files[]" class="custom-file-input" id="validatedCustomFile" multiple="multiple" accept="image/*" data-toggle="tooltip" data-placement="top" title="Please select maximum 6 images" required>
													<label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
												</div>
											
												
											</div>
										</div>
                                        <input type="submit" name="submit" value="Add product" class="btn btn-primary">
                                    </form>
                                    <script>
                                        ! function() {
                                            "use strict";
                                            window.addEventListener("load", function() {
                                                var e = document.getElementById("needs-validation");
                                                e.addEventListener("submit", function(t) {
                                                    !1 === e.checkValidity() && (t.preventDefault(), t.stopPropagation()), e.classList.add("was-validated")
                                                }, !1)
                                            }, !1)
                                        }()
                                    </script>
									
                                </div>
                            </div>
                        </div>
						</div>
					
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © <?php echo date('Y',time());?> ISEBY</a>. All rights reserved.</span></footer>
        </div>
    </div>
	
	
	
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
	
</body>

<script>
	function handleFileSelect(evt) {
    var files = evt.target.files;

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = 
          [
            '<img style="height: 30px; border: 1px solid #000; margin: 5px" src="', 
            e.target.result,
            '" title="', escape(theFile.name), 
            '"/>'
          ].join('');
          
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  document.getElementById('files').addEventListener('change', handleFileSelect, false);
	
</script>

<script>
function getState(val) {
	$.ajax({
	type: "POST",
	url: "ajax.php",
	data:'category_id='+val,
	success: function(data){
		$("#sub_category").html(data);
	}
	});
}

$('#isshipping').click(function () {
    var checked = this.checked;
    console.log(checked);
    $('.check_enable').each(function () {
        $(this).prop('disabled', !checked);
    });
});
</script>
</html>

<?php

function genRandomString($length) {
   $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $string ='IS';    
   $str_len=strlen($characters)-1;
   echo $str_len.'</br>';
   for ($p = 0; $p < $length; $p++) {
	  $string .= $characters[mt_rand(0, $str_len)];
	}
	include('config.php');
	$sql = "SELECT * FROM tbl_products WHERE product_code = '$string'";
	$result = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$count = mysqli_num_rows($result);
	
	if($count == 1) 
	{
		genRandomString($length);
	}else 
	{
		return $string;
	}
  }
?>