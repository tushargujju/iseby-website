<?php
	include('config.php');
	
	$response = array();
	if (isset($_POST['trans_id']) && isset($_POST['user_id']) && isset($_POST['seller_id'])) {
		
		$trans_id = $_POST['trans_id'];
		$user_id = $_POST['user_id'];
		$seller_id = $_POST['seller_id'];
	
		require('u/fpdf.php');

		class PDF extends FPDF
		{
			function Header()
			{
				if(!empty($_FILES["file"]))
				{
					$uploaddir = "temp/test.png";
					$nm = $_FILES["file"]["name"];
					$random = rand(1,99);
					move_uploaded_file($_FILES["file"]["tmp_name"], $uploaddir.$random.$nm);
					$this->Image($uploaddir.$random.$nm,10,10,20);
					unlink($uploaddir.$random.$nm);
				}
				$this->SetFont('Arial','B',12);
				$this->Ln(1);
			}
			function Footer()
			{
				$this->SetY(-15);
				$this->SetFont('Arial','I',8);
				$this->Cell(0,10,'Reg. Address: KK Market, Pune Satara Road, Katraj, Pune - 411041',0,0,'L');
				$this->Cell(0,10,'ISEBY | You see it, You buy it',0,0,'R');
			}
			function ChapterTitle($num, $label)
			{
				$this->SetFont('Arial','',12);
				$this->SetFillColor(200,220,255);
				$this->Cell(0,10,'ISBEY Solutions',0,0,'R',true);
				$this->Ln(0);
			}
			function ChapterTitle2($num, $label)
			{
				$this->SetFont('Arial','',12);
				$this->SetFillColor(249,249,249);
				$this->Cell(0,6,"$num $label",0,1,'L',true);
				$this->Ln(0);
			}

			function WordWrap(&$text, $maxwidth)
			{
				$text = trim($text);
				if ($text==='')
				return 0;
				$space = $this->GetStringWidth(' ');
				$lines = explode("\n", $text);
				$text = '';
				$count = 0;

				foreach ($lines as $line)
				{
					$words = preg_split('/ +/', $line);
					$width = 0;

					foreach ($words as $word)
					{
						$wordwidth = $this->GetStringWidth($word);
						if ($wordwidth > $maxwidth)
						{
							// Word is too long, we cut it
							for($i=0; $i<strlen($word); $i++)
							{
								$wordwidth = $this->GetStringWidth(substr($word, $i, 1));
								if($width + $wordwidth <= $maxwidth)
								{
									$width += $wordwidth;
									$text .= substr($word, $i, 1);
								}
								else
								{
									$width = $wordwidth;
									$text = rtrim($text)."\n".substr($word, $i, 1);
									$count++;
								}
							}
						}
						elseif($width + $wordwidth <= $maxwidth)
						{
							$width += $wordwidth + $space;
							$text .= $word.' ';
						}
						else
						{
							$width = $wordwidth + $space;
							$text = rtrim($text)."\n".$word.' ';
							$count++;
						}
					}
					$text = rtrim($text)."\n";
					$count++;
				}
				$text = rtrim($text);
				return $count;
			}
		}


		$pdf = new PDF();
		$pdf->AliasNbPages();

		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->SetTextColor(32);
		
		$image1 = "assets/static/images/logo_iseby.png";
		$pdf->Cell( 100, 20, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );

		
		$pdf->Cell(0,5,'ISEBY Solutions',0,1,'R');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,5,'GSTIN No. : ABCDE12345WXYZ0',0,1,'R');
		$pdf->Cell(0,5,'Order ID:'.$trans_id,0,1,'R');
		$pdf->Cell(0,3,'',0,1,'L');
		


		$pdf->Cell(190,0,'',1,1,'L');
		
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(90,10,'Date :- '.date('d/m/Y',time()),0,0,'L');
		$pdf->SetFont('Arial','B',18);
		$pdf->Cell(80,10,'Tax Invoice',0,1,'R');
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(190,3,'',0,1,'L');
		$pdf->Cell(95,5,'Buyer name & address',0,0,'L');
		$pdf->Cell(95,5,'Seller name & address',0,1,'L');
		$pdf->SetFont('Arial','',10);



		$x = $pdf->GetX();
		$y = $pdf->GetY();

		$sql2 = "SELECT * FROM tbl_users WHERE id='$user_id'";
		$result2=mysqli_query($conn,$sql2);
		$row2=$result2->fetch_assoc();
		
		$col1="".ucwords($row2['name'])."\n".$row2['mobile'];
		$pdf->MultiCell(95, 5, $col1, 0, 1);

		$pdf->SetXY($x + 95, $y);


		$sql3 = "SELECT * FROM tbl_users WHERE id='$user_id'";
		$result3=mysqli_query($conn,$sql3);
		$row3=$result3->fetch_assoc();

		$sql4 = "SELECT `tbl_seller_info`.*, `tbl_users`.`name`, `tbl_users`.`mobile` FROM `tbl_seller_info`, `tbl_users` WHERE `tbl_seller_info`.`user_id` = '$seller_id' AND `tbl_users`.`id` = `tbl_seller_info`.`user_id`";
		$result4=mysqli_query($conn,$sql4);
		$row4=$result4->fetch_assoc();


		$col2="".ucwords($row4['name'])."\n".ucwords($row4['shop_address'])."\n".$row4['mobile'];
		$pdf->MultiCell(95, 5, $col2, 0);
		$pdf->Ln(0);

		$pdf->Cell(0,5,'',0,1,'L');
		$pdf->SetFillColor(224,235,255);
		$pdf->SetDrawColor(0,0,0);
		$pdf->SetFont('Arial','B',10);
		
		$pdf->Cell(7,6,'#','BT',0,'L',0);
		$pdf->Cell(45,6,'Product name','BT',0,'L',0);
		$pdf->Cell(25,6,'Product code','BT',0,'L',0);
		$pdf->Cell(17,6,'Quantity','BT',0,'L',0);
		$pdf->Cell(25,6,'MRP','BT',0,'L',0);
		$pdf->Cell(23,6,'Discount %','BT',0,'L',0);
		$pdf->Cell(15,6,'Tax %','BT',0,'L',0);
		$pdf->Cell(25,6,'Total','BT',0,'L',0);
		$pdf->Cell(8,6,'','BT',1,'L',0);

		$pdf->SetFont('Arial','',10);
		$sql1="select * from tbl_transaction_products where trans_id='$trans_id'";
		$result1=mysqli_query($conn,$sql1);
		//$row=mysqli_fetch_array($result);
		if($result1->num_rows > 0)
		{
			$i=1;
			$a=0;
			$b=0;
			$c=0;
			while($row1=$result1->fetch_assoc())
			{
				$prod_id1=$row1['prod_id'];
				$sql = "SELECT * FROM tbl_products where id='$prod_id1'";
				$result=$conn->query($sql);
			

				
				while($row=$result->fetch_assoc())
				{
					$pdf->Cell(7,6,$i,'',0,'L',0);
					$pdf->Cell(45,6,ucwords(substr($row['name'],0,20)),'',0,'L',0);
					$pdf->Cell(25,6,$row['product_code'],'',0,'L',0);
					$quantity1=$row1['quantity'];
					$pdf->Cell(17,6,$quantity1,'',0,'L',0);
					$price=$row['price'];
					$a=$a+$price;
					$pdf->Cell(25,6,moneyFormatIndia(number_format((float)$price,2,'.','')),'',0,'L',0);
					$discount_per=$row['discount_per'];
					$pdf->Cell(23,6,$discount_per.'%','',0,'L',0);
					$total1=($price*($discount_per/100));
					$b=$b+$total1;
					$total1=($price-$total1)*$quantity1;
					$c=$c+$total1;
					
					$tax_type2=$row['tax_type'];
					if($tax_type2==0)
					{
						$print_tax="$";
					}
					else
					{
						$print_tax="#";
					}

					$pdf->Cell(15,6,$row['tax_per']." %",'',0,'L',0);
					$pdf->Cell(25,6,moneyFormatIndia(number_format((float)$total1,2,'.','')),'',0,'L',0);
					$pdf->Cell(8,6,$print_tax,'',1,'L',0);
				}
				$i++;
			}
		}

		
		$sql5 = "SELECT * FROM tbl_transaction_buyer WHERE id='$trans_id'";
		$result5=mysqli_query($conn,$sql5);
		$row5=$result5->fetch_assoc();

		$pdf->SetDrawColor(0,0,0);
		//$pdf->Cell(0,2,'',0,1,'L');
		$pdf->Cell(190,0,'',1,1,'L');
		$pdf->Cell(0,2,'',0,1,'L');
		$pdf->Cell(0,5,'Total : '.moneyFormatIndia(number_format((float)$a,2,'.','')),0,1,'R');
		$pdf->Cell(0,5,'Discount : '.moneyFormatIndia(number_format((float)$row5['discount'],2,'.','')),0,1,'R');
		$pdf->Cell(0,5,'Delivery Charges : '.moneyFormatIndia(number_format((float)$row5['delivery_charges'],2,'.','')),0,1,'R');
		$pdf->Cell(0,5,'Sub Total : '.moneyFormatIndia(number_format((float)$row5['trans_amount'],2,'.','')),0,1,'R');
		$pdf->Cell(0,5,'CGST : '.moneyFormatIndia(number_format((float)$row5['tax_total']/2,2,'.','')),0,1,'R');
		$pdf->Cell(0,5,'SGST : '.moneyFormatIndia(number_format((float)$row5['tax_total']/2,2,'.','')),0,1,'R');

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(0,5,'Grand Total : '.moneyFormatIndia(number_format((float)$row5['trans_amount'],2,'.','')),0,1,'R');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(0,5,'Amount in words: '.ucwords(getIndianCurrency($row5['trans_amount'])).' Only',0,1,'L');
		$pdf->Cell(190,0,'',1,1,'L');

		$pdf->SetFont('Arial','',8);

		$pdf->Cell(40,5,'$ = Inclusive tax',0,0,'L');
		$pdf->Cell(40,5,'# = Exclusive tax',0,1,'L');

		$filename="../pdf/".$trans_id.".pdf";
		$pdf->Output($filename,'F');
		
	
		// successfully inserted into database
		$response["success"] = 1;
		$response["message"] = "Product successfully created.";
 
		// echoing JSON response
		echo json_encode($response);
	
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
	 
		// echoing JSON response
		echo json_encode($response);
	}
?>

<html>
<body>
<form method="post">
<input type="text" name="trans_id" autofocus>
<input type="text" name="user_id">
<input type="text" name="seller_id">
<input type="submit" name="submit" value="submit">
</form>

<?php
function moneyFormatIndia($num){

$explrestunits = "" ;
$num=preg_replace('/,+/', '', $num);
$words = explode(".", $num);
$des="00";
if(count($words)<=2){
    $num=$words[0];
    if(count($words)>=2){$des=$words[1];}
    if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
}
if(strlen($num)>3){
    $lastthree = substr($num, strlen($num)-3, strlen($num));
    $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
    $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
    $expunit = str_split($restunits, 2);
    for($i=0; $i<sizeof($expunit); $i++){
        // creates each of the 2's group and adds a comma to the end
        if($i==0)
        {
            $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
        }else{
            $explrestunits .= $expunit[$i].",";
        }
    }
    $thecash = $explrestunits.$lastthree;
} else {
    $thecash = $num;
}
return "$thecash.$des"; // writes the final format where $currency is the currency symbol.

}


function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    
	return ($Rupees ? $Rupees . 'Rupees' : '') . $paise ;
}
?>