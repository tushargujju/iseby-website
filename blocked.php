<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>401 - Access denied</title>
    <link href="style.css" rel="stylesheet">
</head>

<body class="app">
    <div class="pos-a t-0 l-0 bgc-white w-100 h-100 d-f fxd-r fxw-w ai-c jc-c pos-r p-30">
		<div class="row">
		<div class="col-md-4 mb-3 text-center">
			<div class="mR-60"><img alt="#" src="assets/static/images/file.svg" width="200px" height="200px"></div>
		</div>
		<div class="col-md-8 mb-3">
        <div class="d-f jc-c fxd-c">
            <h1 class="mB-30 fw-900 lh-1 c-red-500" style="font-size:56px;">401</h1>
            <h2 class="mB-10 fsz-lg c-grey-900 tt-c">Access denied</h2>
            <p class="mB-30 fsz-def c-grey-800">You are blocked by administrator, due to the violation of our terms and privacy policies, please contact administrator for further enquiries.</p>
            <div><a href="logout.php" type="primary" class="btn btn-primary">Logout</a></div>
        </div>
		</div>
		</div>
    </div>
    <script type="text/javascript" src="vendor.js"></script>
    <script type="text/javascript" src="bundle.js"></script>
</body>

</html>