<?php
include('session.php');

	date_default_timezone_set('Asia/Kolkata');
    $today = date('Y-m-d', time());

    if($expiry_date < $today)
	{
		header('location:401.php');
	}

require('u/fpdf.php');

$trans_id=decrypt_url($_GET['id']);

function decrypt_url($string) {
    $key = "MAL_979877"; //key to encrypt and decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)-ord($keychar));
     $result.=$char;
   }
   return $result;
}
$file = 'pdf/'.$trans_id.'.pdf';
$filename = 'pdf/'.$trans_id.'.pdf';
header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');
@readfile($file);
?>